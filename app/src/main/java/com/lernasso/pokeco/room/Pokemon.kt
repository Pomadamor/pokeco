package com.lernasso.pokeco.room

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull

@Entity(tableName = "pokemon")
class Pokemon(

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    var id: Int = 0,

    @ColumnInfo(name = "imgPokemon")
    var imgPokemon: String = "",

    @ColumnInfo(name = "name")
    var name: String = "",

    @ColumnInfo(name = "description")
    var description: String = ""

) {
    override fun toString(): String {
        return "Pokemon(id=$id, imgPokemon='$imgPokemon', name='$name', description='$description')"
    }
}
