package com.lernasso.pokeco.room

import android.app.Application
import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase

@Database(entities = [(Pokemon::class)], version = 1, exportSchema = false)
abstract class PokemonDataBase : RoomDatabase() {

    abstract fun pokemonDao(): PokemonDao

    companion object {

        private var pokemonDatabase: PokemonDataBase? = null

        fun getDatabase(app: Application): PokemonDataBase {
            if (pokemonDatabase == null) {
                pokemonDatabase =
                    Room.databaseBuilder(app.applicationContext,
                        PokemonDataBase::class.java, "pokemonDatabase").build()
            }
            return pokemonDatabase!!
        }

    }
}
