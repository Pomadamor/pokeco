package com.lernasso.pokeco.room

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*

@Dao
interface PokemonDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg pokemon: Pokemon)

    @Query("SELECT * FROM pokemon")
    fun selectAll(): LiveData<List<Pokemon>>

    @Query("DELETE FROM pokemon WHERE pokemon.id = :id")
    fun delete(id : Int)

    @Update
    fun update(pokemon : Pokemon)

}