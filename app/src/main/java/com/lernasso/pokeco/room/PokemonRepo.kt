package com.lernasso.pokeco.room

import android.app.Application
import android.arch.lifecycle.LiveData

class PokemonRepo(app: Application) {

    companion object {
        var mPokemonRepo: PokemonRepo? = null

        fun getInstance(app: Application): PokemonRepo {
            if (mPokemonRepo == null) {
                mPokemonRepo = PokemonRepo(app)
            }
            return mPokemonRepo!!
        }
    }

    private var mPokemonDao: PokemonDao

    init {
        val pokemonDB = PokemonDataBase.getDatabase(app)
        mPokemonDao = pokemonDB.pokemonDao()
    }

    fun insert(list: List<Pokemon>) {
        mPokemonDao.insert(*list.toTypedArray())
    }

    fun insert(vararg pokemon: Pokemon) {
        mPokemonDao.insert(*pokemon.toList().toTypedArray())
    }

    fun selectAll(): LiveData<List<Pokemon>> {
        return mPokemonDao.selectAll()
    }

    fun delete(pokemon: Pokemon) {
        mPokemonDao.delete(pokemon.id)
    }

    fun update(pokemon: Pokemon) {
        mPokemonDao.update(pokemon)
    }

}
