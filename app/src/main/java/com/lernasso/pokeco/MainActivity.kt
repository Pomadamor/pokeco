package com.lernasso.pokeco

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import com.lernasso.pokeco.room.Pokemon
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    val mPokemonObserver = Observer<List<Pokemon>> {
        it?.let { allPokemons ->
            onPokemonDatabaseUpdate(allPokemons)
        }
    }

    var adapter: PokemonAdapter? = null


    override fun onStart() {
        super.onStart()
        select()
        PokemonManager.allPokemonsLiveData?.observe(this, mPokemonObserver)
    }

    override fun onStop() {
        super.onStop()
        PokemonManager.allPokemonsLiveData?.removeObserver(mPokemonObserver)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        _recyclerView.layoutManager = GridLayoutManager(this, 1)
        adapter = PokemonAdapter { pokemon ->
            val alertChoose = AlertDialog.Builder(this)
            alertChoose.setTitle("Delete or update")
            alertChoose.setPositiveButton("Update") { _, _ ->
                val alertUpdate = AlertDialog.Builder(this)
                alertUpdate.setTitle("Update")
                val vueUpdate = LinearLayout(this)

                val nameEt = EditText(this)
                val imgPokemonEt = EditText(this)
                val descriptionEt = EditText(this)

                nameEt.setText(pokemon.name)
                imgPokemonEt.setText(pokemon.imgPokemon)
                descriptionEt.setText(pokemon.description)

                vueUpdate.addView(nameEt)
                vueUpdate.addView(imgPokemonEt)
                vueUpdate.addView(descriptionEt)

                alertUpdate.setView(vueUpdate)

                alertUpdate.setPositiveButton("Update") { _, _ ->
                    val name = nameEt.text
                    val imgPokemon = imgPokemonEt.text
                    val description = descriptionEt.text

                    pokemon.apply{
                        this.name=name.toString()
                        this.imgPokemon=imgPokemon.toString()
                        this.description=description.toString()
                    }
                }

                alertUpdate.setNegativeButton("Cancel") { dialog, _ ->
                    dialog.dismiss()
                }
                alertUpdate.show()
            }
            alertChoose.setNegativeButton("Delete") { _, _ ->
                PokemonManager.delete(pokemon, application)
            }
            alertChoose.show()
        }

        _recyclerView.adapter = adapter

        _sendBtn.setOnClickListener {
            if (!_name.text.isNullOrEmpty() && !_description.text.isNullOrEmpty()) {

                val pokemon = Pokemon().apply {

                    this.name = _name.text.toString()
                    this.imgPokemon = _imgPokemon.text.toString()
                    this.description = _description.text.toString()

                }
                PokemonManager.insert(pokemon, application)

            } else Toast.makeText(this, "Pas titre ou d'image", Toast.LENGTH_SHORT).show()
        }
    }

    fun select() {
        PokemonManager.selectAll(application)
    }

    private fun onPokemonDatabaseUpdate(list: List<Pokemon>) {

        list.forEach {
            Log.e("Error", "found $it in database")
        }
        adapter?.addAll(list)
    }

}
