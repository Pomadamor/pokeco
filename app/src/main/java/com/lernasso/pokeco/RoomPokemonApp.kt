package com.lernasso.pokeco

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco

class RoomPokemonApp : Application() {

    override fun onCreate() {
        super.onCreate()
        Fresco.initialize(this)

    }
}