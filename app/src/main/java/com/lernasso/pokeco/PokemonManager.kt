package com.lernasso.pokeco


import android.app.Application
import android.arch.lifecycle.LiveData
import android.util.Log
import com.lernasso.pokeco.room.Pokemon
import com.lernasso.pokeco.room.PokemonRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

object PokemonManager {

    var allPokemonsLiveData: LiveData<List<Pokemon>>? = null


    fun selectAll(application: Application) {
        GlobalScope.launch(Dispatchers.Default) {
            allPokemonsLiveData = PokemonRepo.getInstance(application).selectAll()
        }
    }

    fun insertAll(list: List<Pokemon>, app: Application) {
        GlobalScope.launch(Dispatchers.Default) {
            PokemonRepo.getInstance(app).insert(list)
        }
    }

    fun insert(pokemon: Pokemon, app: Application) {
        Log.e("Error", "trying to add $pokemon to DataBase")
        GlobalScope.launch(Dispatchers.Default) {
            PokemonRepo.getInstance(app).insert(pokemon)
        }
    }

    fun delete(pokemon: Pokemon, app: Application) {
        GlobalScope.launch(Dispatchers.Default) {
            PokemonRepo.getInstance(app).delete(pokemon)
        }
    }

    fun update(pokemon: Pokemon, app: Application) {
        GlobalScope.launch {
            PokemonRepo.getInstance(app).update(pokemon)
        }
    }
}