package com.lernasso.pokeco

import android.databinding.DataBindingUtil
import android.databinding.ObservableField
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.lernasso.pokeco.room.Pokemon

typealias PokemonClickListener = (Pokemon) -> Unit

class PokemonAdapter(private val listener: PokemonClickListener?) :
    RecyclerView.Adapter<PokemonAdapter.PokemonViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            layoutInflater, viewType,
            parent, false
        )
        return PokemonViewHolder(binding)
    }

    private val allPokemons = mutableListOf<Pokemon>()

    fun addAll(pokemons: List<Pokemon>) {
        allPokemons.clear()
        allPokemons.addAll(pokemons)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return allPokemons.size
    }

    override fun getItemViewType(position: Int): Int {
        return R.layout.pokemon_list_item
    }

    override fun onBindViewHolder(holder: PokemonViewHolder, position: Int) {
        holder.bind(allPokemons[position], listener)
    }

    class PokemonViewHolder(private val binding: ViewDataBinding) :
        RecyclerView.ViewHolder(binding.root){

        fun bind(pokemon: Pokemon, listener: PokemonClickListener?) {
            binding.setVariable(BR.pokemon, PokemonBinding().apply {
                this.name.set(pokemon.name)
                this.imgPokemon.set(pokemon.imgPokemon)
                this.description.set(pokemon.description)
            })
            binding.root.setOnClickListener {
                listener?.invoke(pokemon)
            }

            binding.executePendingBindings()
        }
    }

}

data class PokemonBinding(
    val name: ObservableField<String> = ObservableField(""),
    val imgPokemon: ObservableField<String> = ObservableField(""),
    val description: ObservableField<String> = ObservableField("")
)